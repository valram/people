from django.contrib.auth.models import User
from django.db import models
from django.urls import reverse
from django.utils.text import slugify


def get_unique_slug(model, field_name, value):
    max_length = model._meta.get_field(field_name).max_length
    slug = slugify(value)
    num = 1
    unique_slug = '{}-{}'.format(slug[:max_length - len(str(num)) - 1], num)
    while model.objects.filter(** {field_name: unique_slug}).exists():
        unique_slug = '{}-{}'.format(slug[:max_length - len(str(num)) - 1], num)
        num += 1
    return unique_slug


class Post(models.Model):
    blog = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=100)
    article = models.TextField(max_length=5000)
    created = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now=True)
    slug = models.SlugField(unique=True, max_length=25, blank=True, editable=False)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = get_unique_slug(self.__class__, 'slug', self.title)
        super(Post, self).save(*args, **kwargs)

    def get_absolute_url(self):
        return reverse(
            'posts:post-detail',
            kwargs={
                'id': self.blog_id,
                'slug': self.slug,
            }
        )

    def get_delete_url(self):
        return reverse(
            'posts:post-delete',
            kwargs={
                'id': self.blog_id,
                'slug': self.slug,
            }
        )

    def __str__(self):
        return '{author} {title} {date}'.format(
            date=self.created,
            author=self.blog.username,
            title=self.title,
        )

