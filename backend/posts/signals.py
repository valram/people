from django.db.models.signals import post_save
from django.dispatch import receiver

from posts.models import Post
from utils.utils import email_followers_new_post_notification


@receiver(post_save, sender=Post)
def send_email(sender, instance, created, **kwargs):
    if created:
        email_followers_new_post_notification(instance)
