from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^(?P<id>\d+)/create/$', views.PostCreateView.as_view(), name='post-create'),
    url(r'^(?P<id>\d+)/posts/$', views.PostListView.as_view(), name='post-list'),
    url(r'^(?P<id>\d+)/posts/following/$', views.FollowingBlogsPostsView.as_view(), name='following-blogs-posts-list'),
    url(r'^(?P<id>\d+)/posts/following/(?P<slug>[\w-]+)$', views.FollowingBlogPostView.as_view(), name='following-blogs-post-detail'),
    url(r'^(?P<id>\d+)/posts/(?P<slug>[\w-]+)/sign/read$', views.PostSignReadView.as_view(), name='post-sign-read'),
    url(r'^(?P<id>\d+)/posts/(?P<slug>[\w-]+)/$', views.PostDetailView.as_view(), name='post-detail'),
    url(r'^(?P<id>\d+)/posts/(?P<slug>[\w-]+)/update/$', views.PostUpdateView.as_view(), name='post-update'),
    url(r'^(?P<id>\d+)/posts/(?P<slug>[\w-]+)/delete/$', views.PostDeleteView.as_view(), name='post-delete'),
]
