from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy, reverse
from django.views import View
from django.views.generic import (
    CreateView,
    ListView,
    DetailView,
    UpdateView,
    DeleteView,
)

from accounts.models import UserActivity
from posts.forms import PostForm
from posts.models import Post


class BlogMixin():
    def get_queryset(self):
        queryset = super(BlogMixin, self).get_queryset()
        return queryset.filter(blog=self.kwargs['id'])


class PostCreateView(CreateView):
    form_class = PostForm
    template_name = 'posts/post_form.html'

    def form_valid(self, form):
        form.instance.blog = self.request.user
        return super(PostCreateView, self).form_valid(form)

    def get_success_url(self):
        return reverse(
            'posts:post-list',
            kwargs={
                'id': self.kwargs['id'],
            }
        )


class PostListView(BlogMixin, ListView):
    model = Post
    queryset = Post.objects.all()


class FollowingBlogsPostsView(ListView):
    model = Post
    queryset = Post.objects.all()

    def get_queryset(self):
        user = self.request.user
        if not user and not user.id == self.kwargs['id']:
            return None
        queryset = super(FollowingBlogsPostsView, self).get_queryset()
        return queryset.filter(
            blog__in=user.activity.following_blogs.all()
            ).exclude(id__in=user.activity.read_posts.all())


class PostSignReadView(View):
    def get(self, request, slug, id, *args, **kwargs):
        toggle_post = get_object_or_404(Post, slug=slug)
        if request.user.is_authenticated():
            user_activity, created = UserActivity.objects.get_or_create(user=request.user)
            if toggle_post in user_activity.read_posts.all():
                user_activity.read_posts.remove(toggle_post)
            else:
                user_activity.read_posts.add(toggle_post)
            return  HttpResponseRedirect(request.META.get('HTTP_REFERER'))


class FollowingBlogPostView(DetailView):
    model = Post


class PostDetailView(BlogMixin, DetailView):
    model = Post


class PostUpdateView(BlogMixin, UpdateView):
    model = Post
    form_class = PostForm

    def get_success_url(self):
        return self.object.get_absolute_url()


class PostDeleteView(BlogMixin, DeleteView):
    model = Post

    def get_success_url(self):
        return reverse(
            'posts:post-list',
            kwargs={
                'id': self.kwargs['id'],
            }
        )
