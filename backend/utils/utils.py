from django.core.mail import EmailMessage
from django.template.loader import render_to_string

from people_project import settings


def email_followers_new_post_notification(post):
    subject = '{user} created new post'.format(user=post.blog.username)
    user_activities = list(post.blog.followed_by.all())
    for user_activity in user_activities:
        if user_activity.user.email:
            html = render_to_string('posts/new_post_email.html', {
                'post': post,
                'followed_by_user': user_activity.user,
            })
            message = EmailMessage(
                subject,
                html,
                from_email=settings.DEFAULT_FROM_EMAIL,
                to=[user_activity.user.email,]
            )
            message.content_subtype = "html"
            message.send()
