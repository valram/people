from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^login/$', views.PeopleLoginView.as_view(), name='login'),
    url(r'^logout/$', views.PeopleLogoutView.as_view(), name='logout'),
    url(r'^blogs/$', views.BlogsList.as_view(), name='blogs-list'),
    url(r'^blogs/(?P<id>\d+)/follow/$', views.FollowBlogView.as_view(), name='follow-blog'),

]
