from django.contrib.auth.models import User
from django.db import models

from posts.models import Post


class UserActivity(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, related_name='activity')
    following_blogs = models.ManyToManyField(User, related_name='followed_by')
    read_posts = models.ManyToManyField(Post, related_name='read_by')
