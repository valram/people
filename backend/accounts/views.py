from django.contrib.auth.models import User
from django.contrib.auth.views import LoginView, LogoutView
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse, reverse_lazy
from django.views import View
from django.views.generic import (
    CreateView,
    ListView,
    DetailView,
    UpdateView,
    DeleteView,
)

from accounts.forms import ReadPostForm, FollowBlogForm
from accounts.models import UserActivity
from posts.models import Post


class PeopleLoginView(LoginView):
    template_name = 'accounts_login.html'

    def get_success_url(self):
        if self.request.user.is_authenticated:
            return reverse(
                'posts:post-list',
                kwargs={
                    'id': self.request.user.id,
                }
            )


class PeopleLogoutView(LogoutView):
    next_page = reverse_lazy('index')


class FollowBlogView(View):
    def get(self, request, id, *args, **kwargs):
        toggle_user = get_object_or_404(User, id=id)
        if request.user.is_authenticated():
            user_activity, created = UserActivity.objects.get_or_create(user=request.user)
            if toggle_user in user_activity.following_blogs.all():
                user_activity.following_blogs.remove(toggle_user)
            else:
                user_activity.following_blogs.add(toggle_user)
        return redirect('accounts:blogs-list')


class BlogsList(ListView):
    model = User
    template_name = 'blogs_list.html'
