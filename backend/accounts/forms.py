from django.forms import ModelForm

from accounts.models import UserActivity


class ReadPostForm(ModelForm):
    class Meta:
        model = UserActivity
        fields = [
            'read_posts',
        ]


class FollowBlogForm(ModelForm):
    class Meta:
        model = UserActivity
        fields = [
            'following_blogs',
        ]
