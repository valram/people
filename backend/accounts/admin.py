from django.contrib import admin

from .models import UserActivity


class UserActivityAdmin(admin.ModelAdmin):
    pass

admin.site.register(UserActivity, UserActivityAdmin)
