### How to start on Linux, python 3.5

```
$ git clone 
$ cd project
$ virtualenv --python=python3.5 .env
$ source .env/bin/activate
$ pip3 install -r requirements.txt

```
Setup database related settings in settings.py

```
$ cd backend
$ python manage.py makemigrations
$ python manage.py migrate
$ python manage.py createsuperuser
$ python manage.py runserver localhost:5000

```

